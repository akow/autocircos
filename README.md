# Automated Circos Plot Generation #

Bash scripts for creating Circos plots from GWAS top pairs list with p-values

## Usage

./makeCircos


## Setup

### Installing Circos (Ubuntu)

* The following instructions were inspired by [here](http://davetang.org/muse/2012/05/08/installing-circos/)


```
#!shell

wget http://circos.ca/distribution/circos-0.67-1.tgz # Or grab the latest version from [here](http://circos.ca/software/download/)
tar xvzf circos-0.67-1.tgz # Extract 
cd circos-0.67-1/bin #change directory into where you unzipped circos and then the bin directory

sudo apt-get -y install libgd2-xpm-dev build-essential 
sudo apt-get -y install libgd-svg-perl

sudo cpan App::cpanminus
sudo cpanm Config::General
sudo cpanm Font::TTF::Font
sudo cpanm GD
sudo cpanm GD::Image
sudo cpanm GD::Polyline
sudo cpanm Math::VecStat
sudo cpanm Readonly
sudo cpanm Regexp::Common
sudo cpanm Text::Format
sudo cpanm Math::Bezier
sudo cpanm Set::IntSpan
sudo cpanm SVG
sudo cpanm Statistics::Basic

test.modules | grep "^fail" #all modules should be installed now, but check for errors
gddiag
chmod +x circos
sudo ln -s `pwd`/circos /usr/bin # Make a link to circos for all to use globally
circos --help # Any additional errors, refer to http://circos.ca/tutorials/lessons/configuration/distribution_and_installation/
```

### Setup for autocircos
```
sudo cpamn Text::CSV_XS
```
git clone https://akow@bitbucket.org/akow/autocircos.git

## Bugs/Questions

* kow.andrew@gmail.com